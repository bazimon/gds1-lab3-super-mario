﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerContoller : MonoBehaviour
{
	public AudioClip breaking;
	public AudioClip coin;
	public AudioClip dying;
	public AudioClip stomp;
	public AudioClip powerUp;
	public AudioClip powerUpSpawn;
    public AudioClip oneUp;
    public AudioClip die;

    public new Transform camera;
    public float cameraEdge;

    private float health;
    private GameController gameController;
    private PlayerMovment playerMovment;
	private GameObject fire;
	private CameraController cameraController;

    //fireBall variables
    public GameObject fireBall;
	public Vector2 offSet = new Vector2 (0.3f, 0.0f);
	private Vector2 velocity = new Vector2 (8, 4);
	private float cooldown = 2;
	private bool fireMario = false;
	private bool canShoot = true;
	public AudioClip fireAudio;

    public Collider2D smallMairoCollider;
    public Collider2D bigMairoCollider;
    private Rigidbody2D currentRigidbody2D;
	private BoxCollider2D boxCollider2D;

	private float random;

	//invincibility states (damaged, star)
	private bool invincibility = false;
	private bool starStruck = false;

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    public enum StateTransition { ToSmall, ToBig, ToFireFlower, ToInvincible, ToVincible };
    public StateTransition CurrentState { get; set; }
    public float transitionTime = 1f;

    private new Rigidbody2D rigidbody2D;
    private float stompValue = 2500;

    // Use this for initialization
    void Start ()
    {
		cameraController = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraController> ();
        playerMovment = GetComponent<PlayerMovment>();
        currentRigidbody2D = GetComponent<Rigidbody2D>();
        smallMairoCollider.enabled = true;
        bigMairoCollider.enabled = false;
        boxCollider2D = GetComponent <BoxCollider2D> ();
        rigidbody2D = GetComponent<Rigidbody2D>();

        fire = GameObject.Find ("Fire");



        health = 1;

        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        GameObject gameControllerOject = GameObject.FindWithTag("GameController");
        if (gameControllerOject != null)
        {
            gameController = gameControllerOject.GetComponent<GameController>();
        }

    }
	
	// Update is called once per frame
	void Update ()
    {

        //checks if Mario has fire mario powerup, spawns fireball at offSet position with a set velocity when T is pressed. Fireball has its own script
        if (fireMario) {
			if ((Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) && canShoot) {
				GameObject go = (GameObject) Instantiate (fireBall, (Vector2)fire.transform.position, Quaternion.identity);
				GetComponent<AudioSource> ().clip = fireAudio;
				GetComponent<AudioSource> ().Play ();
				go.GetComponent<Rigidbody2D> ().velocity = new Vector2 (velocity.x * transform.localScale.x, -velocity.y);
				

			}
		}

		//random is used for random effects, for example if goombas bounce towards right or left when they die
		random = Random.Range (0f, 1f);

        currentRigidbody2D.position = new Vector2
        (
            Mathf.Clamp(currentRigidbody2D.position.x, camera.position.x - cameraEdge, camera.position.x + cameraEdge), 
            currentRigidbody2D.position.y
        );

        AnimatePlayer();
    }


    void AnimatePlayer() {
        if (playerMovment.enabled)
        {
            if (animator.GetBool("idle"))
            {
                //Moving Right
                if (Input.GetAxis("Horizontal") > 0)
                {
                    //spriteRenderer.flipX = false;
					transform.localScale = new Vector2(1 , transform.localScale.y);
                    animator.SetBool("idle", false);
                }
                //Moving Left
                else if (Input.GetAxis("Horizontal") < 0)
                {
                    //spriteRenderer.flipX = true;
					transform.localScale = new Vector2(-1 ,transform.localScale.y);
                    animator.SetBool("idle", false);
                }
            }
            else
            {
                //Not Moving
                if (Input.GetAxis("Horizontal") == 0)
                {
                    animator.SetBool("idle", true);
                }
            }

            //Jumping
            if (!animator.GetBool("jumping"))
            {
                if (Input.GetAxis("Jump") != 0)
                {
                    animator.SetBool("jumping", true);
                }
            }
            else
            {
                if (Input.GetAxis("Jump") == 0) { /////////////////////// <<<<<<< temporary until the jumping in game has been fixed. (currently can mid air jump and junk)
                    animator.SetBool("jumping", false);
                }
            }
        }
    }

    private void StateTransitionMario(StateTransition transitionType) {
        if (transitionType == StateTransition.ToSmall) {
            CurrentState = StateTransition.ToSmall;
            animator.SetBool("large", false);
            animator.SetBool("fireflower", false);
            animator.SetBool("transitioning", true);
            smallMairoCollider.enabled = true;
            bigMairoCollider.enabled = false;
            Time.timeScale = 0;
            StartCoroutine(StopTransitionAfterWait());
        } else if (transitionType == StateTransition.ToBig) {
            CurrentState = StateTransition.ToBig;
            animator.SetBool("large", true);
            animator.SetBool("transitioning", true);
            animator.SetBool("fireflower", false);
            smallMairoCollider.enabled = false;
            bigMairoCollider.enabled = true;
            Time.timeScale = 0;
            StartCoroutine(StopTransitionAfterWait());
        } else if (transitionType == StateTransition.ToFireFlower) {
            CurrentState = StateTransition.ToFireFlower;
            animator.SetBool("fireflower", true);
            animator.SetBool("transitioning", true);
            smallMairoCollider.enabled = false;
            bigMairoCollider.enabled = true;
            Time.timeScale = 0;
            StartCoroutine(StopTransitionAfterWait());
            StartCoroutine(StopTransitionAfterWait());
        } else if (transitionType == StateTransition.ToInvincible) {
            animator.SetBool("invincible", true);
            CurrentState = StateTransition.ToInvincible;
        } else if (transitionType == StateTransition.ToVincible) {
            animator.SetBool("invincible", false);
            CurrentState = StateTransition.ToInvincible;
        }
        }

    private IEnumerator StopTransitionAfterWait() {
        yield return new WaitForSecondsRealtime(transitionTime);
        animator.SetBool("transitioning", false);
        Time.timeScale = 1;
    }


    private void OnCollisionEnter2D(Collision2D collision) {
		Collider2D collider = collision.collider;
		//flower gives fire mario state
		if (collider.tag == "Flower") {
            if(health == 3) { //is fire mario already
                gameController.IncreaseScore(1000, transform.position);
                Destroy(collision.gameObject);
            } else if(health == 2) { //is big mario
                health = 2;
                gameController.IncreaseScore(1000, transform.position);
                fireMario = true;
                StateTransitionMario(StateTransition.ToFireFlower);
				GetComponent<AudioSource> ().clip = powerUp;
				GetComponent<AudioSource> ().Play ();
                Destroy(collider.gameObject);
            } else if (health == 1) { // is small mario
                health += 1;
				GetComponent<AudioSource> ().clip = powerUp;
				GetComponent<AudioSource> ().Play ();
                gameController.IncreaseScore(1000, transform.position);
                StateTransitionMario(StateTransition.ToBig);
                Destroy(collision.gameObject);
            }
		}
		//mushroom ups marios health to a maximum of 2
		if (collider.tag == "Mushroom") {
			if (health < 2) {
				health += 1;
				GetComponent<AudioSource> ().clip = powerUp;
				GetComponent<AudioSource> ().Play ();
                gameController.IncreaseScore(1000, transform.position);
                StateTransitionMario(StateTransition.ToBig);
                Destroy(collision.gameObject);
            } else {
                gameController.IncreaseScore(1000, transform.position);
                Destroy (collision.gameObject);
			}
		}
		//star gives mario starStruck state for 10 seconds
		if (collider.tag == "Star") {
			starStruck = true;
            gameController.IncreaseScore(1000, transform.position);
            StateTransitionMario(StateTransition.ToInvincible);
			cameraController.starBgm ();
            Destroy(collision.gameObject);
			Invoke ("resetStarStruck", 10);
		}
        //one up mushroom does nothing at the moment
        if(collider.tag == "OneUpMushroom") {
            GetComponent<AudioSource>().clip = oneUp;
            GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);
        }
		//as long as mario hasn't been hit in the last x seconds, interact with goombas. If mario hits goombas from the side, he dies, but if he hits goombas from top, they die.
		if (!invincibility) {
			if (collider.tag == "Goomba") {
				Vector3 contactPoint = collision.contacts [0].point;
				Vector3 center = collider.bounds.center;

				bool right = contactPoint.x > center.x;
				bool left = contactPoint.x < center.x;
				//top was hard to define, if you read this and have a better way, please fix. sort of works now. sort of.
				bool top = contactPoint.y > center.y + 0.43;

				//damage mario as long as he isn't starstruck if he collides with goombas from the side. If he has 0 health left, die. If he has fire mario state, remove it. If he survives, set invincibility.
				if (!starStruck) {
					if ((right == true || left == true) && top == false) {
						health -= 1;
                        if (health == 0) {
							death ();
						}
						if (health > 0) {
                            invincibility = true;
							fireMario = false;
                            Invoke("resetInvincibility", 2);
						}
                        if (health == 1) {
                            StateTransitionMario(StateTransition.ToSmall);
                            GetComponent<AudioSource>().clip = die;
                            GetComponent<AudioSource>().Play();
                        }
                        if (health == 2) {
                            StateTransitionMario(StateTransition.ToBig);
                            GetComponent<AudioSource>().clip = die;
                            GetComponent<AudioSource>().Play();
                        }
                    }

				}
                else
                {
					//needed to stop goomba from moving
					collision.gameObject.GetComponent<EnemyMovement> ().enabled = false;
					//randomises if goombas should die towards left or right
					if (random < 0.5f) {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (2, 5, 0);
					} else {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (-2, 5, 0);
					}
					//turns goomba on its head
					collision.gameObject.transform.localScale = new Vector3 (-1f, -1f, 1f);
					//turns trigger on so goomba falls through ground 
					collision.gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
					GetComponent<AudioSource> ().clip = stomp;
					GetComponent<AudioSource> ().Play ();
                    gameController.IncreaseScore(100, transform.position);
                    rigidbody2D.AddForce(new Vector2(0f, stompValue));
                }
				if ((right == false && left == false) && top == true) {
					//Destroy (collision.gameObject);
					collision.gameObject.GetComponent<EnemyMovement> ().enabled = false;
					if (random < 0.5f) {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (2, 5, 0);
					} else {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (-2, 5, 0);
					}
					collision.gameObject.transform.localScale = new Vector3 (-1f, -1f, 1f);
					collision.gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
					GetComponent<AudioSource> ().clip = stomp;
					GetComponent<AudioSource> ().Play ();
                    gameController.IncreaseScore(100, transform.position);
                    rigidbody2D.AddForce(new Vector2(0f, stompValue));
                }
				if ((right == true || left == true) && top == true) {
					//Destroy (collision.gameObject);
					collision.gameObject.GetComponent<EnemyMovement> ().enabled = false;
					if (random < 0.5f) {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (2, 5, 0);
					} else {
						collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (-2, 5, 0);
					}
					collision.gameObject.transform.localScale = new Vector3 (-1f, -1f, 1f);
					collision.gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
                    gameController.IncreaseScore(100, transform.position);
					GetComponent<AudioSource> ().clip = stomp;
					GetComponent<AudioSource> ().Play ();
                    rigidbody2D.AddForce(new Vector2(0f, stompValue));
                }
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		//if mario falls down pit, play death function and remove the hitbox so that he doesn't bounce forever
		if (other.tag == "Pit") {
			Destroy (other);
			death ();
			//SceneManager.LoadScene (0);
		}
		//pick up coins
		if (other.tag == "Coin") {
			GetComponent<AudioSource> ().clip = coin;
			GetComponent<AudioSource> ().Play ();
            gameController.IncreaseCoin(transform.position);
			Destroy (other.gameObject);
		}
			
	}
	//make mario bounce upwards when he dies, falls through objects and set restart state
	public void death() {
        GetComponent<PlayerMovment>().enabled = false; 
		currentRigidbody2D.velocity = Vector3.up * 13;
		gameObject.transform.localScale = new Vector3 (-1f, -1f, 1f);
		if (bigMairoCollider.enabled == true) {
			bigMairoCollider.isTrigger = true;
		} else { 
			smallMairoCollider.isTrigger = true;
		}
		cameraController.soundDying ();
		animator.SetBool("idle", true);
        gameController.RestartLevel();
	}

	void resetInvincibility() {
		invincibility = false;
	}

	void resetStarStruck() {
		starStruck = false;
        StateTransitionMario(StateTransition.ToVincible);
        cameraController.backgroundMusic ();
	}


	//sets a cooldown between shooting
	IEnumerator CanShoot() {
		canShoot = false;
		yield return new WaitForSeconds (cooldown);
		canShoot = true;
	}

	public void breakingBrick() {
		GetComponent<AudioSource> ().clip = breaking;
		GetComponent<AudioSource> ().Play ();
	}

	public void soundCoin() {
		GetComponent<AudioSource> ().clip = coin;
		GetComponent<AudioSource> ().Play ();
	}
		
}
