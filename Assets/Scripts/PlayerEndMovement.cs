﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEndMovement : MonoBehaviour {

    public float speed;

    private new Rigidbody2D rigidbody2D;
    private Vector2 moveDirection;
    public Animator playerAnimator;
    public PlayerContoller playerController;

    // Use this for initialization
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        moveDirection = new Vector2(1, 0);
        transform.localScale = new Vector2(1, transform.localScale.y);
        playerController.enabled = false;
        playerAnimator.enabled = true;
        playerAnimator.SetBool("idle", false);
        playerAnimator.SetBool("jumping", false);
    }


    private void FixedUpdate()
    {
        rigidbody2D.velocity = new Vector2(moveDirection.x * speed, rigidbody2D.velocity.y);
    }
}
