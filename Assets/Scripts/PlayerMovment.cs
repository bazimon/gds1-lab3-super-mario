﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    public Animator playerAnimatior;
    public AudioSource audioSource;
    public AudioClip smallJump;
    public AudioClip bigJump;

    public float moveSpeed;
    public float jumpValue;

    public Transform groundCheck;
    public LayerMask whatIsGround;

    private bool grounded;
    private new Rigidbody2D rigidbody2D;    
    private float groundRadius = 0.2f;
    private float sprint;


    // Use this for initialization
    void Start ()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();

        //jumpValue = transform.position.y;
        //maxJumpValue = jumpValue + maxJump;

    }
	
	// Update is called once per frame
	void Update ()
    {


        if(grounded && Input.GetButtonDown("Jump"))
        {
            rigidbody2D.AddForce(new Vector2(0f, jumpValue));
            if (playerAnimatior.GetBool("large") == true)
            {
                audioSource.PlayOneShot(bigJump);
            }
            else
            {
                audioSource.PlayOneShot(smallJump);
            }
        }

        if (Input.GetButtonUp("Jump"))
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, rigidbody2D.velocity.y * 0.5f);
        }




        // Keep mario from spinging randomly cos unity
        transform.rotation = Quaternion.identity;       
        
    }

    private void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.LeftControl))
        {
            sprint = 3;
        }
        else
        {
            sprint = 0;
        }


        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        float moveHorizontal = Input.GetAxis("Horizontal");

        rigidbody2D.position += new Vector2(moveHorizontal, 0f) * (moveSpeed + sprint) * Time.deltaTime;

        //rigidbody2D.velocity = new Vector2(moveHorizontal * moveSpeed, rigidbody2D.velocity.y);

    }
}
