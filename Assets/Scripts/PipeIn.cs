﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeIn : MonoBehaviour {

    public GameObject player;

    private bool notActivated;

    public AudioSource underground;
    public AudioSource audioSource;
    public AudioClip undergroundTheme;
    public AudioClip pipeSound;

    public Transform playerTransform;
    public CameraController cameraController;

    public BoxCollider2D pipeCollider;

    private void Awake()
    {
        audioSource.enabled = false;
        notActivated = true;
    }    

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (Input.GetAxis("Vertical") < -0.5 && notActivated)
        {
            playerTransform.transform.position = new Vector3(36f, playerTransform.transform.position.y, playerTransform.transform.position.z);
            player.GetComponent<PlayerMovment>().enabled = false;
            player.GetComponent<SpriteRenderer>().sortingOrder = 0;

            pipeCollider.isTrigger = true;
            notActivated = false;

            audioSource.enabled = true;
            audioSource.Play();

            Invoke("TeleportPlayer", 1f);
        }
    }

    void TeleportPlayer()
    {
        player.GetComponent<PlayerMovment>().enabled = true;
        player.GetComponent<SpriteRenderer>().sortingOrder = 1;
        cameraController.MoveToSecretLocation();
        playerTransform.transform.position = new Vector3(95.7f, -8.86f, 0);
        underground.Stop();
        underground.PlayOneShot(undergroundTheme);
    }

}
