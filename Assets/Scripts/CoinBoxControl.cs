﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBoxControl : MonoBehaviour
{   

    public Sprite usedBox;
    public GameObject coin;

    private new Collider2D collider2D;
    private SpriteRenderer spriteRenderer;
    private bool active;
    private GameController gameController;

    void Start ()
    {
        collider2D = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        active = true;

        GameObject gameControllerOject = GameObject.FindWithTag("GameController");
        if (gameControllerOject != null)
        {
            gameController = gameControllerOject.GetComponent<GameController>();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.gameObject.tag == "Player")
        {
            // Get any current contact points on this gameobjects collider
            ContactPoint2D[] contacts = new ContactPoint2D[5];
            collider2D.GetContacts(contacts);

            // If the collison is from the bottom of the collider
            if(contacts[0].normal.y > 0 && active)
            {
                // Increment Score
				PlayerContoller playerContoller = collision.gameObject.GetComponent<PlayerContoller>();
                // Replace with animation,               
                var coinObject = Instantiate(coin, new Vector3(transform.position.x, transform.position.y + 1f), Quaternion.identity);
				playerContoller.soundCoin ();
                Destroy(coinObject, 0.5f);
                gameController.IncreaseCoin(transform.position);

                spriteRenderer.sprite = usedBox;
                active = false;
            }
        }
    }

}
