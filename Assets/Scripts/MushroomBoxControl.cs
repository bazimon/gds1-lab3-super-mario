﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomBoxControl : MonoBehaviour
{
    public Sprite usedBox;
    public GameObject mushroom;
    public GameObject flower;
	public AudioClip spawn;

    private new Collider2D collider2D;
    private SpriteRenderer spriteRenderer;
    private bool active;

    void Start()
    {
        collider2D = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        active = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            // Get any current contact points on this gameobjects collider
            ContactPoint2D[] contacts = new ContactPoint2D[5];
            collider2D.GetContacts(contacts);

            // If the collison is from the bottom of the collider
            if (contacts[0].normal.y > 0 && active)
            {
                if(collision.gameObject.GetComponent<PlayerContoller>().CurrentState == PlayerContoller.StateTransition.ToBig)
                {
                    Instantiate(flower, new Vector3(transform.position.x, transform.position.y + 0.5f), Quaternion.identity);
                }
                else
                {
                    Instantiate(mushroom, new Vector3(transform.position.x, transform.position.y + 0.5f), Quaternion.identity);
                }
                spriteRenderer.sprite = usedBox;
                active = false;
				GetComponent<AudioSource> ().clip = spawn;
				GetComponent<AudioSource> ().Play ();

            }
        }
    }
}
