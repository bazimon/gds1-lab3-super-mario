﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeOut : MonoBehaviour {

    public GameObject player;

    public AudioSource underground;
    public AudioClip undergroundTheme;
    public AudioSource audioSource;
    public AudioClip pipeSound;

    public Transform playerTransform;
    public CameraController cameraController;
    public BoxCollider2D pipeCollider;

    private bool notActivated;
    private Vector3 currentPos;
    private Vector3 newPos;

    private void Awake()
    {
        audioSource.enabled = false;
        notActivated = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetAxis("Horizontal") > 0.2 && notActivated)
        {
            playerTransform.transform.position = new Vector3(playerTransform.transform.position.x, -21.81f, playerTransform.transform.position.z);
            player.GetComponent<PlayerMovment>().enabled = false;
            player.GetComponent<SpriteRenderer>().sortingOrder = 0;

            pipeCollider.isTrigger = true;
            notActivated = false;
            currentPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
            newPos = new Vector3(player.transform.position.x + 2, player.transform.position.y, 0);          

            audioSource.enabled = true;
            audioSource.Play();

            StartCoroutine(Animate());
        }
    }

    void TeleportPlayer()
    {
        player.GetComponent<SpriteRenderer>().sortingOrder = 1;
        cameraController.MoveFromSecretLocation();
        playerTransform.transform.position = new Vector3(143f, -1f, 0);
        player.GetComponent<PlayerMovment>().enabled = true;
        underground.Stop();
        underground.PlayOneShot(undergroundTheme);

    }

    private IEnumerator Animate()
    {
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime)
        {
            player.transform.position = Vector3.Lerp(currentPos, newPos, t);

            yield return true;
        }

        TeleportPlayer();
    }
}
