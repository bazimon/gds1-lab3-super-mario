﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickBoxControll : MonoBehaviour
{

	public AudioClip breaking;

    private new Collider2D collider2D;

    void Start()
    {
        collider2D = GetComponent<Collider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            // Get any current contact points on this gameobjects collider
            ContactPoint2D[] contacts = new ContactPoint2D[5];
            collider2D.GetContacts(contacts);

            // If the collison is from the bottom of the collider
            if (contacts[0].normal.y > 0)
            {
                //TODO Play block hit animation here

                PlayerContoller playerContoller = collision.gameObject.GetComponent<PlayerContoller>();

                // If mario is in large state
                if (playerContoller.CurrentState != PlayerContoller.StateTransition.ToSmall)
                {
					playerContoller.breakingBrick ();
                    Destroy(gameObject);
                    //TODO spawn black braking particles here
                }                           
            }
        }
    }
}
