﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    private Vector3 originalPosition;

	public AudioClip dying;
	public AudioClip bgm;
	public AudioClip star;

    private bool followPlayer;
    private new Camera camera;


	// Use this for initialization
	void Start ()
    {
		GetComponent<AudioSource> ().clip = bgm;
		GetComponent<AudioSource> ().Play ();
        camera = GetComponent<Camera>();


        originalPosition = transform.position;
        followPlayer = true;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if(player.position.x > transform.position.x && followPlayer)
        {
            transform.position = new Vector3(player.position.x, originalPosition.y, originalPosition.z);
        }
    }

    public void MoveToSecretLocation()
    {
        followPlayer = false;
        camera.backgroundColor = Color.black;

        transform.position = new Vector3(101f, -15.9f, -10f);

    }
    public void MoveFromSecretLocation()
    {
        camera.backgroundColor = new Color32(93, 148, 252, 255);
        transform.position = new Vector3(101f, 3.13f, -10f);

        followPlayer = true;
      
    }




    public void soundDying() {
		GetComponent<AudioSource> ().clip = dying;
		GetComponent<AudioSource> ().Play ();
	}

	public void starBgm() {
		GetComponent<AudioSource> ().clip = star;
		GetComponent<AudioSource> ().Play ();
	}

	public void backgroundMusic() {
		GetComponent<AudioSource> ().clip = bgm;
		GetComponent<AudioSource> ().Play ();
	}
}
