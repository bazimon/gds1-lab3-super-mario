﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    public float speed;
    public bool moveLeftFirst;

    private new Rigidbody2D rigidbody2D;
    private Vector2 moveDirection;

    

	// Use this for initialization
	void Start ()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        if(moveLeftFirst)
        {
            moveDirection = new Vector2(-1, 0);
        }
        else
        {
            moveDirection = new Vector2(1, 0);
        }
        
    }
	

    private void FixedUpdate()
    {        
        rigidbody2D.velocity = new Vector2(moveDirection.x * speed, rigidbody2D.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
		if (collision.gameObject.tag != "Player" && collision.gameObject.tag != "Goomba" && collision.gameObject.tag != "Star" && collision.gameObject.tag != "Mushroom" && collision.gameObject.tag != "Coin" )
        {
            var normal = collision.contacts[0].normal;

            if(normal.x > 0)
            {
                moveDirection = new Vector2(1, 0);
            }

            if (normal.x < 0)
            {
                moveDirection = new Vector2(-1, 0);
            }
        }       
    }
}
