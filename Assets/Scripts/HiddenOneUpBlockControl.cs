﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenOneUpBlockControl : MonoBehaviour {

    public Sprite usedBox;
    public GameObject oneUp;

    private new Collider2D collider2D;
    private SpriteRenderer spriteRenderer;
    private bool active;


    void Start() {
        collider2D = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        active = true;
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player" && active) {
            Rigidbody2D playerRigidbod = collider.gameObject.GetComponent<Rigidbody2D>();

            
            if (playerRigidbod.velocity.y > 0 && Physics2D.Raycast(transform.position, Vector3.down, 1)) {
                Instantiate(oneUp, new Vector3(transform.position.x, transform.position.y + 0.5f), Quaternion.identity);
                collider2D.isTrigger = false;
                spriteRenderer.sprite = usedBox;
                active = false;
            }
        }
    }
}
