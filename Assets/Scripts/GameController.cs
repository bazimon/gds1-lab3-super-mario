﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip fastMusic;

    //UI variables
    public Text scoreText;
    public Text timeText;
    public Text coinText;
    public Transform mainCamera;
    public List<GameObject> enemies;
    public GameObject scorePopUpText;

    public Text EndGameText;

    public GameObject canvas;
    public GameObject endGameCanvas;

    private int score;
    private float timeLeft;
    private int coins;
    private bool timerStopped;
    private PlayerContoller playerContoller;

    // Use this for initialization
    void Start ()
    {
        score = 0;
        coins = 0;
        timeLeft = 400;

        endGameCanvas.SetActive(false);

        GameObject playerContollerOject = GameObject.FindWithTag("Player");
        if (playerContollerOject != null)
        {
            playerContoller = playerContollerOject.GetComponent<PlayerContoller>();
        }
    }
	
	void Update ()
    {
             
        UpdateTimeUI();
        //when timer runs out, set restart state
        if (timeLeft == 0)
        {
            playerContoller.death();
        }
        else if(!timerStopped)
        {
            // Apparently mario has it own time system each 2.5 ticks is equivelent to 1 second 
            timeLeft = (160 - Time.timeSinceLevelLoad) * 2.5f;
        }

        foreach(var e in enemies)
        {
            if (e == null)
                continue;

            if(e.transform.position.x - mainCamera.position.x < 25)
            {
                e.SetActive(true);
            }
        }

    }

    private void UpdateScoreUI()
    {
        scoreText.text = "" + score.ToString("000000");
    }

    private void UpdateCoinUI()
    {
        coinText.text = "x" + coins.ToString("00");
    }

    private void UpdateTimeUI()
    {
        
        timeText.text = "" + timeLeft.ToString("000");
        if (timeText.text.ToString() == "100")
        {
            audioSource.Stop();
            audioSource.PlayOneShot(fastMusic);
        }
    }

    public void RestartL()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StopTimer()
    {
        timerStopped = true;
    }

    public void IncreaseScore(int amount, Vector3 scorePosition)
    {

        var scorePopup = Instantiate(scorePopUpText, new Vector3(scorePosition.x, scorePosition.y + 2, scorePosition.z), Quaternion.identity);     
        scorePopup.transform.SetParent(canvas.transform);
        scorePopup.transform.localScale = new Vector3(1, 1, 1);
        scorePopup.GetComponent<Text>().text = amount.ToString();

        score += amount;
        UpdateScoreUI();
    }

    public void IncreaseCoin(Vector3 scorePosition)
    {
        IncreaseScore(200, scorePosition);
        coins++;
        UpdateCoinUI();
    }

    public void RestartLevel()
    {
        Invoke("RestartL", 4f);
    }

    public void ShowEndGameScreen()
    {
        endGameCanvas.SetActive(true);
        EndGameText.text = "Points: " + score;
    }

    public void ConvertTimeToScore()
    {
        StartCoroutine(ConvertTimeToScoreAcync());
    }

    private IEnumerator ConvertTimeToScoreAcync()
    {
        while(timeLeft > 1)
        {
            timeLeft--;            
            score += 100;
            UpdateScoreUI();
            yield return null;
        }
        Invoke("ShowEndGameScreen", 2);
    }

}
