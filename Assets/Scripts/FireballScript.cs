﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour {
	public Rigidbody2D rb;
	private Vector2 velocity;

    private GameController gameController;

	private float random;
	// Use this for initialization
	void Start ()
    {
		Destroy (this.gameObject, 5f);

		rb = GetComponent<Rigidbody2D> ();
		velocity = rb.velocity;

        GameObject gameControllerOject = GameObject.FindWithTag("GameController");
        if (gameControllerOject != null)
        {
            gameController = gameControllerOject.GetComponent<GameController>();
        }
    }
	
	// Update is called once per frame
	void Update () {
		random = Random.Range (0f, 1f);


		if (rb.velocity.y < velocity.y) {
			rb.velocity = velocity;
		}	
	}

	void OnCollisionEnter2D(Collision2D collision) {
		Collider2D collider = collision.collider;

		rb.velocity = new Vector2 (velocity.x, -velocity.y);

		if (collision.contacts [0].normal.x != 0) {
			velocity = new Vector2 (-velocity.x, velocity.y);
			rb.velocity = velocity;
		}

		if (collider.tag == "Pipe") {
			velocity = new Vector2 (-velocity.x, velocity.y);
			rb.velocity = velocity;
		}

		if (collider.tag == "Goomba") {
			collision.gameObject.GetComponent<EnemyMovement> ().enabled = false;
			collision.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (2, 5, 0);
			collision.gameObject.transform.localScale = new Vector3 (-1f, -1f, 1f);
			collision.gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
            gameController.IncreaseScore(100, transform.position);
			death ();
		}

	}

	void death() {
		Destroy (gameObject);
	}
}
