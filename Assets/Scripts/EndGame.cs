﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour {
    
    public AudioSource audioSource;
    public AudioClip flagSound;
    public AudioClip endSound;

    public Animator animator;
    public Rigidbody2D rb2d;

    public GameObject player;
    private Vector3 currentPos;
    private Vector3 newPos;
    private Vector3 endPos;

    public GameController gameController;

	// Use this for initialization
	void Start () {

        GameObject.Find("Player").GetComponent<PlayerEndMovement>().enabled = false;

    }	

    private void OnTriggerEnter2D(Collider2D collision)
    {
        currentPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        newPos = new Vector3(player.transform.position.x, -3, player.transform.position.z);
        endPos = new Vector3(182.5f, -3, player.transform.position.z);
        
        if (collision.tag == "Player")
        {
            float amount = player.transform.position.y;
            gameController.StopTimer();
            CalculateFlagScore(amount);
            rb2d.gravityScale = 1;
            GameObject.Find("Main Camera").GetComponent<AudioSource>().enabled = false;
            GameObject.Find("Player").GetComponent<PlayerMovment>().enabled = false;
            GameObject.Find("Player").GetComponent<Animator>().enabled = false;
            animator.SetBool("endGame", true);
            audioSource.PlayOneShot(flagSound);
            player.transform.position = Vector3.Lerp(currentPos, newPos, Time.deltaTime);
            Invoke("FinishedGame", 1);
        }
    }

    private void CalculateFlagScore(float amount)
    {
        if (amount >= 7)
        {
            gameController.IncreaseScore(5000, transform.position);
        }
        else if (amount > 7 || amount >= 5)
        {
            gameController.IncreaseScore(4000, transform.position);
        }
        else if (amount > 5 || amount >= 3)
        {
            gameController.IncreaseScore(2000, transform.position);
        }
        else if (amount > 3 || amount >= 1)
        {
            gameController.IncreaseScore(800, transform.position);
        }
        else if (amount > 1 || amount >= -1)
        {
            gameController.IncreaseScore(400, transform.position);
        }
        else
        {
            gameController.IncreaseScore(100, transform.position);
        }
    }

    void FinishedGame()
    {
        audioSource.PlayOneShot(endSound);
        GameObject.Find("Player").GetComponent<PlayerEndMovement>().enabled = true;
    }
}
